from cgitb import text
import discord
import datetime
import humanfriendly
import os, json
import yaml

from discord.ext import commands
from discord.commands import permissions, Option
from discord.ui import Button, View

os.chdir("/home/charged/Documents/FreeBot")

with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

guild_ids = int(config["GUILDIDS"])
mod_id = int(config["ModeratorID"])

class Moderation(commands.Cog, name="Moderation"):
  def __init__(self, client):
      self.client = client

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def ban(
    self, ctx,
    member: Option(discord.Member, "Please specify the user.",),
    reason: Option(str, "Enter a reason."),
  ):
    """Bans a member."""
    
    buttonyes = Button(label='Yes', style=discord.ButtonStyle.red)
    buttonno = Button(label='No', style=discord.ButtonStyle.green)
      
    async def buttonyes_callback(interaction):
      await member.ban(reason = reason)
      await interaction.response.edit_message(content=f'{member.mention} has been banned. | Reason: {reason}', view=None)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action cancelled.', view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(f'Are you sure you want to ban {member.mention}?', view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def unban(
    self, ctx,
    member: Option(discord.Member, "Please specify the user.",),
  ):
    """Unbans a member."""
    banned_users = await ctx.guild.bans()
    member_name, member_discriminator = member.split('#')

    for ban_entry in banned_users:
      user = ban_entry.user

      if (user.name, user.discriminator) == (member_name, member_discriminator):
        await ctx.guild.unban(user)
        await ctx.respond(f'Unbanned {user.mention}!')
        return

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def kick(
        self, ctx,
      member: Option(discord.Member, "Please specify the user.",),
      reason: Option(str, "Enter a reason."),
  ):
    """Kicks a member."""
    
    buttonyes = Button(label='Yes', style=discord.ButtonStyle.red)
    buttonno = Button(label='No', style=discord.ButtonStyle.green)
      
    async def buttonyes_callback(interaction):
      await member.kick(reason = reason)
      await interaction.response.edit_message(content=f'{member.mention} has been kicked. | Reason: {reason}', view=None)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action Cancelled.', view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(f'Are you sure you want to kick {member.mention}?', view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def timeout(
    self, ctx, *,
    member: Option(discord.Member, "Please specify the user.",),
    reason: Option(str, "Enter a reason."),
    time: Option(str, "Enter a duration of the timeout."),
  ):
    """Timeouts a member."""

    embed = discord.Embed(
      title='Confirmation',
      description=f'You would like to timeout **{member.mention}** for {time}. \nReason: {reason}',
      color=0xe74c3c
    )

    time = humanfriendly.parse_timespan(time)

    buttonyes = Button(label='Yes', style=discord.ButtonStyle.red)
    buttonno = Button(label='No', style=discord.ButtonStyle.green)
      
    async def buttonyes_callback(interaction):
      await member.timeout(until = discord.utils.utcnow() + datetime.timedelta(seconds=time), reason=reason)
      await interaction.response.edit_message(content=f'{member.mention} has been timed out. | Reason: {reason}', embed=None,  view=None)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action Cancelled.', embed=None, view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def remove_timeout(self, ctx, user: discord.Member=None, *, reason=None):
    """Removes a timeout from a member."""
    await user.timeout(until=None, reason=reason)
    await ctx.respond(f'Timeout has been removed for {user}!')

  @commands.command()
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def purge(self, ctx, amount=10):
    """Purges specified messages. Default = 10"""
    await ctx.channel.purge(limit=amount)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def warnings(
    self, ctx,
    user: Option(discord.User, "Please specify the user.")
  ):
    """Checks a user's warnings!"""
    await self.open_account(user)
    users = await self.get_warnings()

    warnings = users[str(user.id)]['warningscount']

    embed = discord.Embed(
      title = f"{user}'s Warnings",
      color = 0xe74c3c
    )

    embed.add_field(
      name = 'Warnings',
      value = warnings
    )

    embed.set_footer(text="Reasons might come soon...")

    await ctx.respond(embed = embed)

  async def open_account(self, user):

    users = await self.get_warnings()

    if str(user.id) in users:
      return False
    else:
      users[str(user.id)] = {}
      users[str(user.id)]['warningscount'] = 0

    with open('warnings.json', 'w') as f:
      json.dump(users, f)
    return True

  async def get_warnings(self):
    with open ('warnings.json', 'r') as f:
      users = json.load(f)

    return users

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def warn(
    self, ctx,
    user: Option(discord.User, "Please specify the user."),
    reason: Option(str, "Enter the reason for the warning.")
  ):
    """Warn a user."""

    embed = discord.Embed(
      title='You have been warned in FreeTech Studios',
      description=f'__Reason:__ {reason}',
      color=0xe74c3c
    )

    embed.set_footer(text='If you believe this is a mistake, please contact staff.')

    buttonyes = Button(label='Yes', style=discord.ButtonStyle.green)
    buttonno = Button(label='No', style=discord.ButtonStyle.red)
      
    async def buttonyes_callback(interaction):
      await self.open_account(user)

      users = await self.get_warnings()

      warnstogive = 1

      users[str(user.id)]['warningscount'] += warnstogive
      
      await interaction.response.edit_message(content=f'Warned {user.mention} | Reason: {reason}', view=None)
      await user.send(embed=embed)

      with open('warnings.json', 'w') as f:
          json.dump(users, f)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action cancelled.', embed=None, view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(f'**Confirmation:** \nYou would like to warn {user.mention} | Reason: {reason}', view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_any_role('👮‍♂️ Moderator', '👮‍♂️ Trial Moderator')
  async def unwarn(
    self, ctx,
    user: Option(discord.User, "Please specify the user.")
  ):
    """Unwarn a user."""
    buttonyes = Button(label='Yes', style=discord.ButtonStyle.green)
    buttonno = Button(label='No', style=discord.ButtonStyle.red)
      
    async def buttonyes_callback(interaction):
      await self.open_account(user)

      users = await self.get_warnings()

      warnstogive = 1

      users[str(user.id)]['warningscount'] -= warnstogive
      
      await interaction.response.edit_message(content=f'Unwarned {user.mention}', view=None)

      with open('warnings.json', 'w') as f:
          json.dump(users, f)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action cancelled.', embed=None, view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(f'**Confirmation:** \nYou would like to unwarn {user.mention}.', view=view)

def setup(client):
  client.add_cog(Moderation(client))
