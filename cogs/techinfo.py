import discord
import yaml

from discord.ext import commands
from discord.ui import Button, View
from discord.commands import permissions

with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

guild_ids = int(config["GUILDIDS"])
fte = int(config["FreeTech-ExpertsID"])

class Techinfo(commands.Cog, name="Techinfo"):
  def __init__(self, client):
      self.client = client

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def dxvk(self, ctx):
      """Links to DXVK tutorials"""
      embed = discord.Embed(
          title = 'DXVK Install for Linux',
          description='Click [here](https://github.com/lutris/docs/blob/master/HowToDXVK.md) to get directed to install DXVK on your system.',
          color=0xe74c3c
      )

      button = Button(label='DXVK', style=discord.ButtonStyle.url, url='https://github.com/lutris/docs/blob/master/HowToDXVK.md')

      view = View(button)

      await ctx.respond(embed = embed, view = view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def safemode(self, ctx):
      """Learn how to boot your PC in Safe Mode"""
      embed = discord.Embed(
          title = 'Booting Your PC in Safe Mode (Windows 10)',
          description='Click [here](https://support.microsoft.com/en-us/windows/start-your-pc-in-safe-mode-in-windows-10-92c27cff-db89-8644-1ce4-b3e5e56fe234) or the button below to learn how to start your PC in safemode.',
          color=0xe74c3c
      )

      button = Button(label='Article', style=discord.ButtonStyle.url, url='https://support.microsoft.com/en-us/windows/start-your-pc-in-safe-mode-in-windows-10-92c27cff-db89-8644-1ce4-b3e5e56fe234')

      view = View(button)

      await ctx.respond(embed = embed, view = view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def linuxdistro(self, ctx):
      """Sends an embed with links to download different linux distros."""
      embed = discord.Embed(title='Linux Distros', description='Select a link to download a linux distro!', color=0xe74c3c)
      embed.add_field(name='Mint', value='https://www.linuxmint.com/download.php')
      embed.add_field(name='Manjaro', value='https://manjaro.org/download/')
      embed.set_footer(text='More coming soon!')

      button2 = Button(label='Mint', style=discord.ButtonStyle.url, url='https://www.linuxmint.com/download.php')
      button4 = Button(label='Manjaro', style=discord.ButtonStyle.url, url='https://manjaro.org/download/')

      view = View(button2, button4)

      await ctx.respond(embed=embed, view=view)
      
  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def linuxinstall(self, ctx):
      """Sends a tutorial to help you install linux."""
      embed = discord.Embed(title="How to install Linux (Most Distros)",
      description = "**You will need:** \n A working computer, with an OS installed and a USB drive (8GB) \n \n 1. Select the Linux Distro you want to use (`!distros` for a list of download links). \n \n 2. Use `!balenaetcher` to install Etcher, our flashing tool we'll use. \n \n 3. Open Etcher, and select your downloaded ISO file, your USB will already have been detected. Click Flash! \n \n 4. Once Flashing complete, restart your PC and spam your bootkey. Then Select your USB. \n \n 5. Boot into your Distro, and open the Installer \n \n 6. Go through the Installtion, then Enjoy!",
      color = 0xe74c3c)
      embed.set_footer(text="Let us know if we can impove on commands like this!")
      await ctx.respond(embed=embed)
      
  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def windowsinstall(self, ctx):
      """Sends a tutorial to help you install Windows."""
      embed = discord.Embed(title="How to install Windows 10 from a Bootable USB",
      description = "**You will need:** \n A working computer, with an OS installed and a USB drive (8GB) \n \n 1. Download the Windows Media Creation tool, and run it: \n https://www.microsoft.com/en-gb/software-download/windows10 \n \n 2. Select Create Installation Media, when prompted. \n  \n 3. Select Edition, Language and others. \n \n 4. Select your USB. \n \n 5. Wait for the Flashing proccess to finish. \n \n 6. Restart the Computer, and spam your bootkey. \n \n 7. Go through the setup, enjoy!",
      color = 0xe74c3c)
      embed.set_footer(text="Let us know if we can impove on commands like this!")

      button = Button(label='Windows', style=discord.ButtonStyle.url, url='https://www.microsoft.com/en-gb/software-download/windows10')
      
      view = View(button)

      await ctx.respond(embed=embed, view=view)
      
  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def windows(self, ctx):
      """Sends a link to the Windows 10 download page."""
      embed = discord.Embed(title="Windows 10 Installer Download Link:",
      description = "https://www.microsoft.com/en-gb/software-download/windows10",
      color = 0xe74c3c)
      embed.set_footer(text="Use `!windowsinstall` for instructions.")

      button = Button(label='Windows', style=discord.ButtonStyle.url, url='https://www.microsoft.com/en-gb/software-download/windows10')
      
      view = View(button)
      
      await ctx.respond(embed=embed, view=view)
      
  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def balenaetcher(self, ctx):
      """Sends a link to download Balena Etcher."""
      embed = discord.Embed(title="Balena Etcher Download Link",
      description = "https://balena.io/etcher",
      color = 0xe74c3c)
      embed.set_footer(text="Enjoy!")

      button = Button(label='Etcher', style=discord.ButtonStyle.url, url='https://balena.io/etcher')
      
      view = View(button)

      await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def malwarebytes(self, ctx):
      """Sends a link to download MalwareBytes antivirus."""
      embed = discord.Embed(title="Malware Bytes Download Link",
      description = "Keep your PC free from Viruses with Malwarebytes! \n \n **Download Link:** \n https://www.malwarebytes.com/",
      color = 0xe74c3c)
      embed.set_footer(text="Stay safe online!")

      button = Button(label='MalwareBytes', style=discord.ButtonStyle.url, url='https://www.malwarebytes.com/')
      
      view = View(button)

      await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def winusbforlinux(self, ctx):
      """Learn how to create a bootable UEFI Windows 11 USB from Linux."""
      pageone = discord.Embed(
          title = "How to create a bootable Windows 11 (UEFI) USB from Linux",
          description = "This process requires [GParted](https://gparted.org/), or a partition manager with the same capabilities. \nClick The arrow keys to move to the next page. \n\n**Requires:** 8GB+ USB Stick \n\n1.) Plug in your USB drive. \n3.) Open your partition manager, and make sure your USB is visible. \n3.) Unmount all the existing partitions on the USB, if there are any. \n4.) Create a new GPT partition table. \n5.) Create 2 partitions: \n> - The first partition should have a FAT32 filesystem with the size of 1024MB. Label it 'BOOT.' \n> - For the second partition, the filesystem should be NTFS with the size of the remaining storage of the USB. Label: 'INSTALL' \n6.) Save your changes, make sure both partitions are mounted after the process is complete.",
          color = 0xe74c3c
      )

      pageone.set_footer(text = "Click the arrow buttons to view different pages! | Page 1/2")

      pagetwo = discord.Embed(
          title = "How to create a bootable Windows 11 (UEFI) USB from Linux",
          description = "7.) Download a [Windows 11 ISO](https://www.microsoft.com/en-ca/software-download/windows11). \n8.) Mount the ISO, the view its contents to confirm. \n9.) Copy the contents of the ISO except for the sources folder into the BOOT partition. \n> Create a your own sources folder in the BOOT partition. \n> Copy the `boot.wim` file from the ISO's sources file to the BOOT partition's ISO file. \n10.) Copy all the contents from the ISO file to the INSTALL partition of the USB. \n11.) You're done! To boot into the USB, restart your PC and spam your bootkey. Then select your USB flash drive.",
          color = 0xe74c3c
      )

      pagetwo.set_footer(text = "Page 2/2")

      async def left_callback(interaction):
          await interaction.response.edit_message(embed = pageone, view = view)

      async def right_callback(interaction):
          await interaction.response.edit_message(embed = pagetwo, view = view)

      left = Button(label='⬅️', style=discord.ButtonStyle.blurple)
      right = Button(label='➡️', style=discord.ButtonStyle.blurple)

      view = View(left, right)

      left.callback = left_callback
      right.callback = right_callback

      await ctx.respond(embed = pageone, view = view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def unsupportedos(self, ctx):
      """Learn why you shouldn't use Windows 7 and other unsupported operating systems."""
      embed = discord.Embed(
        title='Notice',
        description='We do not give support to those with unsupported operating systems such as Windows 8 and below. \nThis is because these Operating Systems no longer recieve security updates and can be dangerous to use.',
        color=0xe74c3c
      )
      embed.set_footer(text='Note: We do offer help in upgrading to a new OS from and unsupported one!')
      await ctx.respond(embed=embed)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def tronscript(self, ctx):
      """Links to the TronScript subreddit."""
      embed = discord.Embed(
      title='Tronscript Link',
      description='https://www.reddit.com/r/TronScript/',
      color=0xe74c3c
      )
      embed.set_footer(text='Suggested by Ethan Alexander.')

      button = Button(label='Tronscript', style=discord.ButtonStyle.url, url='https://www.reddit.com/r/TronScript/')
      
      view = View(button)

      await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role(fte)
  async def bootkey(self, ctx):
      """Lists the bootkeys for popular brands."""
      embed = discord.Embed(
        title='Bootkeys for Popular Manufacturers',
        description='You may need this for things like booting into live USBs.',
        color=0xe74c3c
      )
      embed.add_field(
          name="Apple",
          value="Option"
      )
      embed.add_field(
          name="ASUS, Samsung, Sony",
          value="Esc"
      )
      embed.add_field(
          name="Acer, Dell, Huawei, Lenovo, Samsung, Toshiba",
          value="F12"
      )
      embed.add_field(
          name="HP",
          value="F9"
      )
      embed.add_field(
          name="Intel, Sony",
          value="F10"
      )
      embed.add_field(
          name="MSI, Sony",
          value="F11"
      )
      embed.add_field(
          name="Samsung",
          value="F2"
      )
      embed.set_footer(text='Nothing here works for you? Ask a staff member!')
      await ctx.respond(embed=embed)

def setup(client):
  client.add_cog(Techinfo(client))
