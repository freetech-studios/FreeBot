from discord.ext import commands
from better_profanity import profanity

# profanity.load_censor_words_from_file("profanity.txt")

class Listeners(commands.Cog, name="Listeners"):
  def __init__(self, client):
      self.client = client

"""
  @commands.Cog.listener()
  async def on_message(self, message):
    if not message.author.bot:
      if profanity.contains_profanity(message.content):
        await message.delete()
"""

def setup(client):
  client.add_cog(Listeners(client))